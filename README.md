#Sum two numbers

_Sum two numbers_ is a simple page with two input fields. On button click, the script should display the sum value of the entered numbers. Default fields values: 0.1 and 0.2.

**Example:**

input: 0.1, 0.2

result: 0.3

###Installation##

```$ git clone https://denisdmitriyeff@bitbucket.org/denisdmitriyeff/sum-two-numbers.git```

or simply upload all given files to your computer.

###How to start

Open **index.html** file in your browser.