document.querySelector('.btn').addEventListener('click', function() {
  const firstNumber = document.getElementById('fnumber').value;
  const secondNumber = document.getElementById('snumber').value;
  // const result = parseFloat(firstNumber) + parseFloat(secondNumber);
  const result = (parseFloat(firstNumber) * 10 + parseFloat(secondNumber) * 10) / 10;

  //A decimal point and nulls are added (if needed), to create the specified length
  document.getElementById('result').innerHTML = result.toPrecision(1);

});
